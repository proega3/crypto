import Vue from 'vue';
import App from './app/App.vue';
import { router } from './_helpers';
//import { store } from './_store';
import './registerServiceWorker';
import * as VueGoogleMaps from "vue2-google-maps";
import VueSweetalert2 from 'vue-sweetalert2';

Vue.config.productionTip = false;

/*
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
*/
Vue.use(VueSweetalert2);
Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyArvu1LlgrJyhzqtX901DYdczuWm6bHmHw",
        libraries: "places" // necessary for places input
    }
});

new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
