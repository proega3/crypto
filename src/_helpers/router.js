import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '../views/HomePage'
import LoginPage from '../views/LoginPage'
import DashboardPage from '../views/DashboardPage'
import SmartTradePage from '../views/SmartTradePage'
import BotsPage from '../views/BotsPage'
import MyBotsPage from '../views/MyBotsPage'
import MyExchanges from '../views/MyExchangesPage'
import Marketplace from '../views/MarketPlacePage'
import TradeDiary from '../views/TradeDiaryPage'
import InviteFriends from '../views/InviteFriendsPage'
import Subscription from '../views/SubscriptionPage'
Vue.use(Router);

export const router = new Router({
    mode: 'history',
    routes: [
        { path: '/', component: HomePage, name: 'Home' },
        { path: '/login', component: LoginPage, name: 'Login' },
        { path: '/dashboard', component: DashboardPage, name: 'DashboardView' },
        { path: '/smartpage', component: SmartTradePage, name: 'SmartTradeView' },
        { path: '/bots', component: BotsPage, name: 'BotsView' },
        { path: '/mybots', component: MyBotsPage, name: 'MyBotsView' },
        { path: '/myexchanges', component: MyExchanges, name: 'MyExchangesView' },
        { path: '/marketplace', component: Marketplace, name: 'MarketplaceView' },
        { path: '/tradediary', component: TradeDiary, name: 'TradeDiaryView' },
        { path: '/invitefriends', component: InviteFriends, name: 'InviteFriendsView' },
        { path: '/subscription', component: Subscription, name: 'SubscriptionView' },
        // otherwise redirect to home
        { path: '*', redirect: '/' }
    ]
});
