export function host() {

    if (process.env.NODE_ENV !== 'production') {
        return "http://localhost:8000";
    }else{
        return "";
    }
}